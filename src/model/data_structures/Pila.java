package model.data_structures;

import java.util.Iterator;

public class Pila<T extends Comparable <T>> implements Iterable<T>, IStack<T>
{
	private Node first; // top of stack (most recently added node)
	private int N; // number of items
	private class Node
	{ // nested class to define nodes
		T item;
		Node next;
	}
	public boolean isEmpty() { return first == null; } // Or: N == 0.
	public int size() { return N; }
	public void push(T item)
	{ // Add item to top of stack.
		Node oldfirst = first;
		first = new Node();
		first.item = item;
		first.next = oldfirst;
		N++;
	}
	public T pop()
	{ // Remove item from top of stack.
		T item = first.item;
		first = first.next;
		N--;
		return item;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	private class ListIterator implements Iterator<T>
	{
		private Node current = first;
		public boolean hasNext()
		{ return current != null; }
		public void remove() { }
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		}
	}
}
