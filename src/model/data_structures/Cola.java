package model.data_structures;

import java.util.Iterator;

public class Cola<T extends Comparable <T>> implements Iterable<T>, IQueue<T>
{
	private Node first; // link to least recently added node
	private Node last; // link to most recently added node
	private int N; // number of items on the queue
	private class Node
	{ // nested class to define nodes
		T item;
		Node next;
	}
	public boolean isEmpty() { return first == null; } // Or: N == 0.
	public int size() { return N; }

	@Override
	public void enqueue (T item)
	{ // Add item to the end of the list.
		Node oldlast = last;
		last = new Node();
		last.item = item;
		last.next = null;
		if (isEmpty()) first = last;
		else oldlast.next = last;
		N++;
	}
	public T dequeue()
	{ // Remove item from the beginning of the list.
		T item = first.item;
		first = first.next;
		if (isEmpty()) last = null;
		N--;
		return item;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	private class ListIterator implements Iterator<T>
	{
		private Node current = first;
		public boolean hasNext()
		{ return current != null; }
		public void remove() { }
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		}
	}

}