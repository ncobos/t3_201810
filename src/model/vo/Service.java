package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;
	
	private String tripStartTime;

	private int dropoffCommunityArea;

	private int dropoffCensusTract;

	private int dropoffCentroidLongitude;

	Taxi taxi = null;

	public Service (String pTripId, int pSec, double pMil, double pTot, int pComArea, String taxiId, String pCom, String pStart)
	{
		tripId = pTripId;
		
		tripSeconds = pSec;
		
		tripMiles = pMil;
		
		tripTotal = pTot;
		
		dropoffCommunityArea = pComArea;
		
		tripStartTime = pStart;
		
		taxi = new Taxi(taxiId, pCom);
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi.getTaxiId();
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		int rta = 1;
		
		if(tripId.equals(o.tripId))
		{
			rta = 0;
		}
		
		return rta;
	}
	
	public int getDropComArea()
	{
		return dropoffCommunityArea;
	}
	
	public Taxi getTaxi()
	{
		return taxi;
	}


	/**
	 * @return total - Total cost of the trip
	 */
	public String getTripStartTime() {
		// TODO Auto-generated method stub
		return tripStartTime;
	}

	
}
