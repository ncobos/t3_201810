package model.logic;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Pila;
import model.vo.Taxi;
import model.vo.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	Cola <Service> colaJson = new Cola();

	Pila <Service> pilaJson = new Pila();

	public void loadServices(String serviceFile, String pTaxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + pTaxiId);

		//		Cola <Service> colaJson = new Cola <Service>();
		//
		//		Pila <Service> pilaJson = new Pila <Service>();

		JsonParser parser = new JsonParser();

		try {

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));


			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null )
				{
					System.out.println("El objeto es nulo en: " + i);
				}

				/* Obtener la propiedad taxi ID de un servicio (String) */
				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null)
				{ taxiId = obj.get("taxi_id").getAsString(); }


				if(pTaxiId.equals(taxiId))
				{
					/* Obtener la propiedad company de un servicio (String) */
					String company = "NaN";
					if ( obj.get("company") != null)
					{ company = obj.get("company").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
					String dropoff_centroid_longitude = "NaN";
					if ( obj.get("dropoff_centroid_longitude") != null )
					{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
					String dropoff_centroid_latitude = "NaN";
					if ( obj.get("dropoff_centroid_latitude") != null )
					{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					//					/* Obtener la propiedad dropoff_centroid_location de un servicio (String)*/
					//					String dropoff_centroid_location = "NaN";
					//					if ( obj.get("dropoff_centroid_latitude") != null )
					//					{ dropoff_centroid_location = obj.get("dropoff_centroid_location").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad trip ID de un servicio (String) */
					String tripId = "NaN";
					if ( obj.get("trip_id") != null)
					{ tripId = obj.get("trip_id").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad trip seconds de un servicio (int) */
					int tripSec = 0;
					if ( obj.get("trip_seconds") != null)
					{ tripSec = obj.get("trip_seconds").getAsInt(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad trip miles de un servicio (double) */
					double tripMil= 0;
					if ( obj.get("trip_miles") != null)
					{ tripMil = obj.get("trip_miles").getAsDouble(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad trip total de un servicio (double) */
					double tripTot= 0;
					if ( obj.get("trip_total") != null)
					{ tripTot = obj.get("trip_total").getAsDouble(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					//					/* Obtener la propiedad taxi ID de un servicio (String) */
					//					String taxiId = "NaN";
					//					if ( obj.get("taxi_id") != null)
					//					{ taxiId = obj.get("taxi_id").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad dropoff community area de un servicio (int) */
					int comArea = 0;
					if ( obj.get("dropoff_community_area") != null)
					{ comArea = obj.get("dropoff_community_area").getAsInt(); }
					//
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					/* Obtener la propiedad dropoff census tract de un servicio (int) */
					//					String dropoffCensusTract = "NaN";
					//					if ( obj.get("dropoff_census_tract") != null )
					//					{ dropoffCensusTract = obj.get("dropoff_census_tract").getAsString(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//
					//					/* Obtener la propiedad extras de un servicio (int) */
					//					String extras = "NaN";
					//					if ( obj.get("extras") != null)
					//					{ extras = obj.get("extras").getAsString(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//
					//					/* Obtener la propiedad fare de un servicio (double) */
					//					double fare= 0;
					//					if ( obj.get("fare") != null)
					//					{ fare = obj.get("fare").getAsDouble(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//
					//					/* Obtener la propiedad payType de un servicio (String) */
					//					String payType= "NaN";
					//					if ( obj.get("payment_type") != null)
					//					{ payType = obj.get("payment_type").getAsString(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad pickup_census_tract de un servicio (String) */
					//					String pickup_census_tract= "NaN";
					//					if ( obj.get("pickup_census_tract") != null)
					//					{ pickup_census_tract = obj.get("pickup_census_tract").getAsString(); }
					//					
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad pickup_centroid_latitude de un servicio (String) */
					//					String pickup_centroid_latitude= "NaN";
					//					if ( obj.get("pickup_centroid_latitude") != null)
					//					{ pickup_centroid_latitude = obj.get("pickup_centroid_latitude").getAsString(); }
					//					
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad pickup_centroid_location de un servicio (String) */
					//					String pickup_centroid_location= "NaN";
					//					if ( obj.get("ppickup_centroid_location") != null)
					//					{ pickup_centroid_location = obj.get("pickup_centroid_location").getAsString(); }
					//					
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad pickup_centroid_longitude de un servicio (String) */
					//					String pickup_centroid_longitude= "NaN";
					//					if ( obj.get("pickup_centroid_longitude") != null)
					//					{ pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad pickup_community_area de un servicio (String) */
					String pickup_community_area= "NaN";
					if ( obj.get("pickup_community_area") != null)
					{ pickup_community_area = obj.get("pickup_community_area").getAsString(); }

					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad tips de un servicio (int) */
					//					String tips = "NaN";
					//					if ( obj.get("tips") != null)
					//					{ tips = obj.get("tips").getAsString(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad tolls de un servicio (int) */
					//					String tolls = "NaN";
					//					if ( obj.get("tolls") != null)
					//					{ tolls = obj.get("tolls").getAsString(); }
					//
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//					
					//					/* Obtener la propiedad trip_end_timestamp de un servicio (String) */
					//					String trip_end_timestamp= "NaN";
					//					if ( obj.get("trip_end_timestamp") != null)
					//					{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }
					//					
					//					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad  trip_start_timestamp de un servicio (String) */
					String  trip_start_timestamp= "0";
					if ( obj.get("trip_start_timestamp") != null)
					{  trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


					JsonObject dropoff_localization_obj = null; 

					//System.out.println("(Lon= "+dropoff_centroid_longitude+ ", Lat= "+dropoff_centroid_latitude +") (Datos String)" );

					/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
					if ( obj.get("dropoff_centroid_location") != null )
					{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
					JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					/* Obtener cada coordenada del JsonArray como Double */
					double longitude = dropoff_localization_arr.get(0).getAsDouble();
					double latitude = dropoff_localization_arr.get(1).getAsDouble();}
					//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");

					//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



					Service servicio = new Service(tripId, tripSec, tripMil, tripTot, comArea, taxiId, company, trip_start_timestamp);

					colaJson.enqueue(servicio);
					pilaJson.push(servicio);
				}
			}


			System.out.println("En esta lista hay: " + colaJson.size() +" en la cola y " + pilaJson.size() + " en la pila.");


		}

		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		System.out.println("pilaJson.size()");
		int [] resultado = new int[2];

		Pila <Service> pilaOrden = new Pila <Service>();

		Pila <Service> pilaNoOrden = new Pila <Service>();

		Service servicio1 = pilaJson.pop();

		pilaOrden.push(servicio1);

		while (pilaJson.isEmpty()==false)
		{
			Service servicioOrdenado = pilaOrden.pop();

			Service servicioAOrdenar = pilaJson.pop();

			String ordenado = servicioOrdenado.getTripStartTime();

			String noOrdenado = servicioAOrdenar.getTripStartTime();

			System.out.println(ordenado.compareToIgnoreCase(noOrdenado));

			System.out.println("1: " + ordenado);
			System.out.println("2: " + noOrdenado);

			if(ordenado.compareToIgnoreCase(noOrdenado)>0)
			{
				pilaOrden.push(servicioOrdenado);
				pilaOrden.push(servicioAOrdenar);
			}

			else
			{
				pilaOrden.push(servicioOrdenado);
				pilaNoOrden.push(servicioAOrdenar);
			}
		}

		resultado[0]= pilaOrden.size();
		resultado[1] = pilaNoOrden.size();
		return resultado;
	}

	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];

		Pila <Service> pilaOrden = new Pila <Service>();

		Cola<Service> colaNoOrden = new Cola <Service>();

		Cola<Service> colaFinal = new Cola <Service>();


		Service servicio1 = colaJson.dequeue();

		pilaOrden.push(servicio1);

		while (colaJson.isEmpty()!= true)
		{
			Service servicioOrdenado = pilaOrden.pop();

			Service servicioAOrdenar = colaJson.dequeue();

			String ordenado = servicioOrdenado.getTripStartTime();

			String noOrdenado = servicioAOrdenar.getTripStartTime();
			
			System.out.println("1 :" + ordenado);
			System.out.println("2: " + noOrdenado);


			if(ordenado.compareToIgnoreCase(noOrdenado)<0)
			{
				pilaOrden.push(servicioOrdenado);

				pilaOrden.push(servicioAOrdenar);

			}

			else
			{
				pilaOrden.push(servicioOrdenado);
				colaNoOrden.enqueue(servicioAOrdenar);
			}
		}

		resultado[0]= pilaOrden.size();
		resultado[1] = colaNoOrden.size();
		return resultado;
	}


}
